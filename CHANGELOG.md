# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.5

- patch: Update the Readme with a new Atlassian Community link.

## 0.3.4

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.3.3

- patch: Added code style checks

## 0.3.2

- patch: Documentation updates

## 0.3.1

- patch: Internal maintenance: update pipes toolkit version

## 0.3.0

- minor: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.2.2

- patch: Minor documentation updates

## 0.2.1

- patch: Updated contributing guidelines

## 0.2.0

- minor: Fix passing of client_url and source inside incident details.
- minor: Removed unused pipe variables from the README.md

## 0.1.2

- patch: Fixed colourised logging.

## 0.1.1

- patch: Fixed missing variables

## 0.1.0

- minor: Initial release
- minor: Initial release of the pipe

